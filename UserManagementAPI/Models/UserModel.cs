﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserManagementAPI.Models
{
    public class UserModel
    {
        public string userId { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
    }
}