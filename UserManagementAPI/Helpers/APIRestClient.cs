﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using UserManagementAPI.Models;

namespace UserManagementAPI.Helpers
{
    public interface IApiRestClient
    {
        Task<UserModel> PostUser(UserModel id);
    }

    public class APIRestClient:IApiRestClient
    {
        private const string url = "https://jsonplaceholder.typicode.com/";
        public static HttpClient CreateClient()
        {
            //while implementng authentication
            //if (!string.IsNullOrWhiteSpace(accessToken))
            //{
            //    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
            //}
            var client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
        public Task<UserModel> PostUser(UserModel userModel)
        {
            var accessToken = string.Empty;
            using (var client = CreateClient())
            {
                var response = client.PostAsJsonAsync(url+"posts", userModel).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return null;
                }
                JsonSerializerSettings serSettings = new JsonSerializerSettings();
                serSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                if (response.IsSuccessStatusCode)
                {
                    var user = JsonConvert.DeserializeObject<UserModel>(response.Content.ReadAsStringAsync().Result, serSettings);
                    return Task.FromResult(user);
                }
                else
                    return Task.FromResult<UserModel>(null);
            }
        }
    }
}