﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace UserManagementAPI.CustomAttributes
{
    public class TokenAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //base.OnAuthorization(actionContext);
            if (actionContext.Request.Headers.GetValues("Authorization") != null)
            {
                IEnumerable<string> headerValues;
                string token = string.Empty;
                if (actionContext.Request.Headers.TryGetValues("Authorization", out headerValues))
                {
                    token = headerValues.FirstOrDefault();
                }
                else
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    return;

                }
                if (!string.IsNullOrEmpty(token))
                {
                    //validate base64enoded
                    string[] tokenvalue = token.Split(' ');
                    if (tokenvalue.Length<=1)
                    {
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                        return;
                    }
                    byte[] data = System.Convert.FromBase64String(tokenvalue[1]);
                    string decodedString = Encoding.UTF8.GetString(data);

                    var encodedToken = System.Convert.ToBase64String(Encoding.UTF8.GetBytes(decodedString + ":1"));
                    HttpContext.Current.Response.AddHeader("AuthenticationToken", encodedToken);
                    HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
                }
                else
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    return;

                }
            }
        }
    }
}