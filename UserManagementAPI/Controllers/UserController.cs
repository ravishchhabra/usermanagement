﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using UserManagementAPI.CustomAttributes;
using UserManagementAPI.Helpers;
using UserManagementAPI.Models;

namespace UserManagementAPI.Controllers
{
    [EnableCorsAttribute("*","*","POST")]
    [TokenAuthorize]
    public class UserController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage PostUser([FromBody] UserModel userModel)
        {
            try
            {
                //Rest Client
                var apiRestCLient = new APIRestClient();
                var task = apiRestCLient.PostUser(userModel);
                task.Wait();
                var result = task.Result;
                var message = Request.CreateResponse(HttpStatusCode.OK, userModel);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
            finally
            {
                //_unitOfWork.Dispose();
            }
        }


    }
}
