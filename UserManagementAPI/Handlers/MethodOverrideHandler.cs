﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace UserManagementAPI.Handlers
{
    public class MethodOverrideHandler : DelegatingHandler
    {
        readonly string[] _methods = { "GET", "DELETE", "HEAD", "PUT", "OPTIONS" };
        const string _header = "Access-Control-Request-Method";

        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            // Check for HTTP POST with the X-HTTP-Method-Override header.
            //if (request.Method == HttpMethod.Post && request.Headers.Contains(_header))
            //{
                // Check if the header value is in our methods list.                
                var method = request.Method.Method;
                if (_methods.Contains(method, StringComparer.InvariantCultureIgnoreCase))
                {
                    // Change the request method.
                    request.Method = new HttpMethod(method);
                    var response = new HttpResponseMessage(HttpStatusCode.MethodNotAllowed);
                    tsc.SetResult(response);   // Also sets the task state to "RanToCompletion"
                }
            return tsc.Task;
        }
    }
}