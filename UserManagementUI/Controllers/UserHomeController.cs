﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UserManagementUI.Helpers;
using UserManagementUI.Models;
using static UserManagementUI.CustomAttribute.ActionParameterAttribute;

namespace UserManagementUI.Controllers
{
    [Authorize]
    public class UserHomeController : Controller
    {
        private IApiRestClient _apiRestClient;

        public UserHomeController(IApiRestClient apiRestClient)
        {
            _apiRestClient = apiRestClient;
        }

        // GET: UserHome
        [NewUserId(id = "1")]
        public ActionResult Details(Int32 id)
        {
            ViewData["UserName"] = System.Web.HttpContext.Current.User.Identity.Name;
            Task<IEnumerable<UserModel>> user = _apiRestClient.GetUserPosts(id);
            user.Wait();
            return View(user.Result.Take(5));
        }

        // GET: UserHome
        [NewUserId(id = "1")]
        public ActionResult Index(Int32 id)
        {
           Task<object> user = _apiRestClient.GetUserContent(id);
            user.Wait();
            ViewBag.User = user.Result;
            var username = ViewBag.User.username;
            return View();
        }

        public ActionResult CreateUser(UserModel userModel)
        {
            if (string.IsNullOrEmpty(userModel.title))
                ModelState.AddModelError("Title", "Title is Required.");
            if (string.IsNullOrEmpty(userModel.body))
                ModelState.AddModelError("Body", "Body is Required.");
            if (ModelState.IsValid)
            {
                Task<UserModel> glossary = _apiRestClient.PostUser(userModel);
                glossary.Wait();

            }
            else
                return View(userModel);
            return RedirectToAction("Index");
        }
    }
}