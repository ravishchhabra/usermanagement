﻿using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UserManagementUI.Models;

namespace UserManagementUI.Helpers
{
    public interface IApiRestClient
    {
        Task<UserModel> PostUser(UserModel userModel);
        Task<IEnumerable<UserModel>> GetUserPosts(int id);
        Task<object> GetUserContent(int userId);
    }
    public class ApiRestClient : IApiRestClient
    {
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        private static string url = Convert.ToString(ConfigurationManager.AppSettings["WebAPIUrl"]);
        private static readonly string accessToken = "T3JjaGFyZEFwaUtleQ==";

        public static HttpClient CreateClient(string apiurl)
        {
            //while implementng authentication
            var client = new HttpClient();
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
            }
            
            client.BaseAddress = new Uri(apiurl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
        public Task<UserModel> PostUser(UserModel userModel)
        {
            
            using (var client = CreateClient(url))
            {
                var response = client.PostAsJsonAsync(url + "User/PostUser", userModel).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return null;
                }
                JsonSerializerSettings serSettings = new JsonSerializerSettings();
                serSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                if (response.IsSuccessStatusCode)
                {
                    var glossary = JsonConvert.DeserializeObject<UserModel>(response.Content.ReadAsStringAsync().Result, serSettings);
                    return Task.FromResult(glossary);
                }
                else
                    return Task.FromResult<UserModel>(null);
            }
        }

        public Task<IEnumerable<UserModel>> GetUserPosts(int userId)
        {
            var apiUrl = Convert.ToString(ConfigurationManager.AppSettings["GenericWebAPIUrl"]);
            using (var client = CreateClient(apiUrl))
            {
                var response = client.GetAsync(apiUrl +"posts?userId="+userId).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return null;
                }
                JsonSerializerSettings serSettings = new JsonSerializerSettings();
                serSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                if (response.IsSuccessStatusCode)
                {
                    var lstuser = JsonConvert.DeserializeObject<IEnumerable<UserModel>>(response.Content.ReadAsStringAsync().Result, serSettings);
                    return Task.FromResult(lstuser);
                }
                else
                    return Task.FromResult<IEnumerable<UserModel>>(null);
            }
        }

        public Task<object> GetUserContent(int userId)
        {
            var apiUrl = Convert.ToString(ConfigurationManager.AppSettings["GenericWebAPIUrl"]);
            using (var client = CreateClient(apiUrl))
            {
                var response = client.GetAsync(apiUrl + "users/" + userId).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return null;
                }
                JsonSerializerSettings serSettings = new JsonSerializerSettings();
                serSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                if (response.IsSuccessStatusCode)
                {
                    var lstuser = JsonConvert.DeserializeObject<object>(response.Content.ReadAsStringAsync().Result, serSettings);
                    return Task.FromResult(lstuser);
                }
                else
                    return Task.FromResult<object>(null);
            }
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}