﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserManagementUI.Models
{
    public class UserModel
    {

        public string userId { get; set; }
        public int id { get; set; }

        [Display(Name = "Enter Definition")]
        [Required(ErrorMessage = "Please Enter Term Definition")]
        public string title { get; set; }

        [Display(Name = "Enter Definition")]
        [Required(ErrorMessage = "Please Enter Term Definition")]
        public string body { get; set; }

    }
}