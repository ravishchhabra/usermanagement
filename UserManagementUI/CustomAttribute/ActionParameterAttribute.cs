﻿using System;
using System.Web.Mvc;

namespace UserManagementUI.CustomAttribute
{
    public class ActionParameterAttribute
    {
        public class NewUserIdAttribute : ActionFilterAttribute
        {
            public string id { get; set; }
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                
                if (filterContext.ActionParameters.ContainsKey("id"))
                {
                    filterContext.ActionParameters["id"] =Convert.ToInt32(id);
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }
}